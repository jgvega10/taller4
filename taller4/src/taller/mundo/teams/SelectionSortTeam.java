package taller.mundo.teams;

/*
 * SelectionSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;

import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class SelectionSortTeam extends AlgorithmTeam
{
     public SelectionSortTeam()
     {
          super("Selection Sort (-)");
          userDefined = false;
     }

     @Override
     public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
     {
          return selectionSort(list, orden);
     }

     /**
      Ordena un arreglo de enteros, usando Ordenamiento por selección.
      @param arr Arreglo de enteros.
    **/
    private Comparable[] selectionSort(Comparable[] arr, TipoOrdenamiento orden)
    {
    	// Para hacer en casa  
    	
    	
    	int indicador = 0;
    	
    	if(orden.compareTo(TipoOrdenamiento.ASCENDENTE) == 0)
    	{
    		for (int i = 0; i < arr.length; i++)
    		{
    			Comparable primero = arr[i];
    			
    			for (int j = 1; j < arr.length; i++) 
        		{
        			Comparable comparar = arr[j];
        			
        			if( primero.compareTo(comparar) == -1)
        			{
        				indicador = j;
        			}
    			}
    			
    			if(i != indicador)
    			{
    				Comparable cambio = arr[i];
        			
        			arr[i] = arr[indicador];
        			arr[indicador] = cambio;
    			}
			}
    	}
    	else
    	{
    		for (int i = 0; i < arr.length; i++)
    		{
    			Comparable primero = arr[i];
    			
    			for (int j = 1; j < arr.length; i++) 
        		{
        			Comparable comparar = arr[j];
        			
        			if( primero.compareTo(comparar) == 1)
        			{
        				indicador = j;
        			}
    			}
    			
    			if(i != indicador)
    			{
    				Comparable cambio = arr[i];
        			
        			arr[i] = arr[indicador];
        			arr[indicador] = cambio;
    			}
			}
    	}
    	return arr;
    }
    
    public String mio()
    {
    	String a = "";
    	Comparable[] arr = {9,-10,4,7,-20 };
    	Comparable[] b = selectionSort(arr, TipoOrdenamiento.ASCENDENTE);
    	
    	for (int i = 0; i < b.length; i++) {
			Comparable c = b[i];
			a += c;
		}
    	
    	System.out.print(a);
    	return a;
    }

}
