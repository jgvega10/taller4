package taller.test;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.InsertionSortTeam;
import taller.mundo.teams.SelectionSortTeam;
import junit.framework.TestCase;

public class InsertionSortTeamTest extends TestCase
{
	private InsertionSortTeam insertion;

	public void sortTest()
	{
		insertion = new InsertionSortTeam();
		Comparable[] arreglo ={9,-10,4,7,-20 };

		Comparable[] res = insertion.sort(arreglo, TipoOrdenamiento.ASCENDENTE);

		assertEquals("El numero en la primera posicion deberia ser el mayor", 9,res[0]);

		Comparable[] res2 = insertion.sort(arreglo, TipoOrdenamiento.DESCENDENTE);

		assertEquals("El numero en la primera posicion deberia ser el menor", -20,res[0]);

	}
}
