package taller.test;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.BubbleSortTeam;
import taller.mundo.teams.SelectionSortTeam;
import junit.framework.TestCase;

public class BubbleSortTest  extends TestCase
{
	private BubbleSortTeam bubble;

	public void sortTest()
	{
		bubble = new BubbleSortTeam();
		Comparable[] arreglo ={9,-10,4,17,-20 };

		Comparable[] res = bubble.sort(arreglo, TipoOrdenamiento.ASCENDENTE);
		
		assertEquals("El numero en la primera posicion deberia ser el mayor", 17,res[0]);
		Comparable[] res2 = bubble.sort(arreglo, TipoOrdenamiento.DESCENDENTE);
		
		assertEquals("El numero en la primera posicion deberia ser el menor", -20,res[0]);
	}

}
