package taller.test;

import junit.framework.TestCase;
import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.SelectionSortTeam;

public class SelectionSortTeamTest extends TestCase
{
	private SelectionSortTeam selection;

	public void sortTest()
	{
		selection = new SelectionSortTeam();
		Comparable[] arreglo ={9,-10,4,7,-20 };

		Comparable[] res = selection.sort(arreglo, TipoOrdenamiento.ASCENDENTE);

		assertEquals("El numero en la primera posicion deberia ser el mayor", 9,res[0]);

		Comparable[] res2 = selection.sort(arreglo, TipoOrdenamiento.DESCENDENTE);

		assertEquals("El numero en la primera posicion deberia ser el menor", -20,res[0]);

	}
}
